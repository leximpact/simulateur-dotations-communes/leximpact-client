# Utilisation de LexImpact Client avec Docker

Cette documentation ne concerne que l'exécution du client, en utilisant le server de production. Pour l'usage avec leximpact-server local, voir leximpact-server/docker/DOCKER.md.

## Pre-requis

Cloner le projet client dans un dossier et se rendre dans leximpact-client.
```sh
git clone https://git.leximpact.dev/leximpact/leximpact-client.git
cd leximpact-client
```

Préparer la configuration :
```sh
cp docker/docker-remote.env .env
```
Les paramètres par défaut fonctionnent, voir le README principale pour l'explication des paramètres.

## Lancement

```sh
docker-compose up
```
Ceci va exécuter :
 - leximpact-client [http://localhost:9080](http://localhost:9080)

Arrêter avec ctrl+c

## Forcer le build
```sh
docker-compose up --build
```

## Nettoyage
Efface les conteneurs, les volumes et les images :
```sh
docker-compose down -v
docker image rm leximpact-client_leximpact_client_only
```

## Debug

Check Javascript code
```
docker exec leximpact-client_leximpact_client_only_1 npm run code:check
```

Fix linting
```
docker exec leximpact-client_leximpact_client_only_1 npm run code:fix
```

Get a terminal inside the running client container :
```
docker exec -i docker-leximpact_leximpact_client_1 /bin/bash
```

View open port :
```
netstat -lntp
```

Kill node :
```
kill -9 $(ps aux | grep '\snode\s' | awk '{print $2}')
```