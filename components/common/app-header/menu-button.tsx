import MenuIcon from "@material-ui/icons/Menu";

import { HeaderButton } from "./header-button";


interface Props {
  isMobile: boolean;
}

function HeaderMenuButton({ isMobile }: Props) {
  const onClick = (e) => {
    e.preventDefault();
    window.location.replace("/");
  };

  return (
    <HeaderButton
      caption="Accueil"
      icon={<MenuIcon fontSize="small" />}
      isMobile={isMobile}
      onClick={onClick} />
  );
}

export default HeaderMenuButton;
