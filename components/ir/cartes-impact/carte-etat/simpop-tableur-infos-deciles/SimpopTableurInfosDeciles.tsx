import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Tooltip from "@material-ui/core/Tooltip";
import { Fragment } from "react";
// eslint-disable-next-line no-unused-vars
import { RootState } from "../../../../../redux/reducers";
import { formatNumber } from "../../../../common";
import styles from "./SimpopTableurInfosDeciles.module.scss";

const NOMBRE_DECILES = 10;
let id = 0;

interface Props {
  deciles: RootState["results"]["totalPop"]["deciles"];
  frontieresDeciles: RootState["results"]["totalPop"]["frontieresDeciles"];
}

function createFromDeciles(index: number, decile: Props["deciles"][0], frontiereDecile: Props["frontieresDeciles"][0]) {
  let impactMoyenFoyerPlf: string | undefined;
  if (typeof decile.plf === "number") {
    impactMoyenFoyerPlf = decile.avant
      ? formatNumber(Math.round((decile.plf / decile.avant - 1) * 100))
      : "-";
  }

  let impactMoyenFoyerReforme: string | undefined;
  if (typeof decile.apres === "number") {
    impactMoyenFoyerReforme = decile.avant
      ? formatNumber(Math.round((decile.apres / decile.avant - 1) * 100))
      : "-";
  }
  let ratioAvantSurRfr: string | undefined;
  if (typeof decile.avg_irpp_on_avg_rfr_avant === "number") {
    ratioAvantSurRfr = formatNumber(Math.round(
      decile.avg_irpp_on_avg_rfr_avant * 10000
      )/100);
  }

  let ratioPlfSurRfr: string | undefined;
  if (typeof decile.avg_irpp_on_avg_rfr_plf === "number") {
    //let ratio : number;
    ratioPlfSurRfr = formatNumber(Math.round(
      decile.avg_irpp_on_avg_rfr_plf * 10000
      )/100);
  }

  let ratioApresSurRfr: string | undefined;
  if (typeof decile.sum_rfr === "number" && typeof decile.plf === "number" && typeof decile.apres === "number") {
    //let ratio : number;
    ratioApresSurRfr = formatNumber(Math.round(
      (decile.apres) / decile.sum_rfr * 10000
      )/100);
  }
  let ratioEcartApresMoinsAvantSurRfr: string | undefined;
  if (typeof decile.apres === "number") {
    ratioEcartApresMoinsAvantSurRfr = formatNumber(Math.round(
      (decile.apres - decile.avant) / decile.sum_rfr * 10000
      )/10);
  }

  let ratioEcartPlfMoinsAvantSurRfr: string | undefined;
  if (typeof decile.sum_rfr === "number" && typeof decile.plf === "number") {
    ratioEcartPlfMoinsAvantSurRfr = formatNumber(Math.round(
      (decile.plf - decile.avant) / decile.sum_rfr * 10000
      )/10);
  }

  let ratioEcartApresMoinsPlfSurRfr: string | undefined;
  if (typeof decile.sum_rfr === "number" && typeof decile.plf === "number" && typeof decile.apres === "number") {
    ratioEcartApresMoinsPlfSurRfr = formatNumber(Math.round(
      (decile.apres - decile.plf) / decile.sum_rfr * 10000
      )/10);
  }


  const impotMoyenFoyer = formatNumber(Math.round(decile.avg_irpp_avant));
  const rfrMoyenFoyer = formatNumber(Math.round(decile.avg_rfr));

  let impotMoyenFoyerPlf: string | undefined;
  if (typeof decile.avg_irpp_plf === "number") {
    impotMoyenFoyerPlf = formatNumber(Math.round(decile.avg_irpp_plf));
  }

  let impotMoyenFoyerReforme: string | undefined;
  if (typeof decile.avg_irpp_apres === "number") {
    impotMoyenFoyerReforme = formatNumber(Math.round(decile.avg_irpp_apres));
  }

  const recettesEtat = formatNumber(Math.round(decile.avant / 10000000) / 100);
  let recettesEtatPlf: string | undefined;
  if (typeof decile.plf === "number") {
    recettesEtatPlf = formatNumber(Math.round(decile.plf / 10000000) / 100);
  }

  let recettesEtatReforme: string | undefined;
  if (typeof decile.apres === "number") {
    recettesEtatReforme = formatNumber(Math.round(decile.apres / 10000000) / 100);
  }
  const frontiere = index + 1 < NOMBRE_DECILES ? formatNumber(Math.round(frontiereDecile)) : "-";

  id += 1;
  return {
    decile: index + 1,
    frontiereDecile: frontiere,
    id,
    rfrMoyenFoyer,
    ratioAvantSurRfr,
    ratioPlfSurRfr,
    ratioApresSurRfr,
    ratioEcartApresMoinsAvantSurRfr,
    ratioEcartPlfMoinsAvantSurRfr,
    ratioEcartApresMoinsPlfSurRfr,
    impactMoyenFoyerPlf,
    impactMoyenFoyerReforme,
    impotMoyenFoyer,
    impotMoyenFoyerPlf,
    impotMoyenFoyerReforme,
    recettesEtat,
    recettesEtatPlf,
    recettesEtatReforme,
  };
}

function imageDecile(decileId) {
  const imageId = `imageDecile${decileId}`;
  const imagePath = `/static/images/decile${decileId}.png`;
  return <img key={imageId} alt="" height="24" src={imagePath} width="24" />;
}

export function SimpopTableurInfosDeciles({ deciles, frontieresDeciles }: Props) {
  const rows = deciles.map(
    (currElement, index) => createFromDeciles(index, currElement, frontieresDeciles[index]),
  );
  const NON_APPLICABLE = "—";

  return (
    <Table>
      <TableHead>
        <TableRow classes={{ root: styles.tableHeader }}>
          <TableCell classes={{ root: styles.cellStyle }}>
            <b>Décile</b>
          </TableCell>
          <TableCell classes={{ root: styles.cellStyle }}>
            <b>Revenu&nbsp;fiscal de&nbsp;référence</b>
            <br />
            (limite supérieure)
          </TableCell>
          <TableCell classes={{ root: styles.cellStyle }}>
            <b>Impôt moyen des foyers</b>
            <br />
            (par an)
          </TableCell>
          <TableCell classes={{ root: styles.cellStyle }}>
            <b>Recettes pour l&apos;État</b>
            <br />
            (en Milliards d&apos;euros)
          </TableCell>
          <TableCell classes={{ root: styles.cellStyle }}>
            <b>Part de l'impôt<br/>par rapport au RFR des foyers</b>
          </TableCell>
          <TableCell classes={{ root: styles.cellStyle }}>
            <b>Part de l'évolution de l'impôt<br/>par rapport au RFR des foyers</b><br/>En € pour mille €.
          </TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {rows.map(row => (
          <TableRow key={row.id}>
            <TableCell classes={{ root: styles.cellStyle }}>
              {imageDecile(row.decile)}
            </TableCell>
            <TableCell classes={{ root: styles.cellStyle }}>
              &#10877;&nbsp;
              {row.frontiereDecile === "-" ? (
                <span className={styles.infinity}>∞</span>
              ) : (
                row.frontiereDecile
              )}
              €/an<br/>

            </TableCell>
            <TableCell classes={{ root: styles.cellStyle }}>
              <span className={styles.base}>
                {row.impotMoyenFoyer}
                €
              </span>
              &nbsp;
              {
                row.impotMoyenFoyerPlf === undefined
                  ? null
                  : (
                    <span className={styles.plf}>
                      {row.impotMoyenFoyerPlf}
                      €
                    </span>
                  )
              }
              &nbsp;
              {
                row.impotMoyenFoyerReforme === undefined
                  ? null
                  : (
                    <span className={styles.reform}>
                      {row.impotMoyenFoyerReforme}
                      €
                    </span>
                  )
              }
            </TableCell>
            <TableCell classes={{ root: styles.cellStyle }}>
              <span className={styles.base}>
                {row.recettesEtat}
                Md€
              </span>
              &nbsp;
              {
                row.recettesEtatPlf === undefined
                  ? null
                  : (
                    <span className={styles.plf}>
                      {row.recettesEtatPlf}
                      Md€
                    </span>
                  )
              }
              &nbsp;
              {
                row.recettesEtatReforme === undefined
                  ? null
                  : (
                    <span className={styles.reform}>
                      {row.recettesEtatReforme}
                      Md€
                    </span>
                  )
              }
            </TableCell>
            <TableCell classes={{ root: styles.cellStyle }}>
            <Tooltip classes={{
                      popper: styles.tooltipContainer,
                      tooltip: styles.tooltipContent,
                    }}
                    title={(<Fragment>{"On divise le montant moyen de l'impôt par la moyenne des RFR du décile, qui est de : "}<strong>{`${row.rfrMoyenFoyer} €`}</strong> € /an</Fragment>)}>
            <div>
              { row.ratioAvantSurRfr === undefined
                ? null
              : (
                <span className={styles.base}>
                      {row.ratioAvantSurRfr} %
                </span>
              )
              }
              {
                row.ratioPlfSurRfr === undefined
                ? null
                : (
                  <span className={styles.plf}>
                        {row.ratioPlfSurRfr === "-"
                          ? NON_APPLICABLE
                          : `${row.ratioPlfSurRfr} %`}
                      </span>
                )
              }
              {
                row.ratioApresSurRfr === undefined
                  ? null
                  : (

                    <span className={styles.reform}>
                      {row.ratioApresSurRfr === "-"
                        ? NON_APPLICABLE
                        : `${row.ratioApresSurRfr} %`}
                    </span>
                  )
              }

              </div>
              </Tooltip>
            </TableCell>
            <TableCell classes={{ root: styles.cellStyle }}>

            <div>
              {
                // If PLF, display diff with avant
                (row.ratioEcartPlfMoinsAvantSurRfr === undefined)
                ? null
              : (
                <Tooltip classes={{ popper: styles.tooltipContainer, tooltip: styles.tooltipContent }}
                title={
                (<Fragment>
                {"On soustrait le montant de la loi en vigueur au montant du PLF"}<br/>
                {" et on divise le résultat par le RFR moyen qui est de"}<br />
                <strong>{`${row.rfrMoyenFoyer} €`}</strong>
                {"  /an pour ce décile."}</Fragment>)}>
                  <span className={styles.plf}>
                    {`${row.ratioEcartPlfMoinsAvantSurRfr} ‰`}
                  </span>
                </Tooltip>)
              }
              {
                // If reform and PLF, display diff with PLF
                row.ratioEcartApresMoinsPlfSurRfr === undefined
                  ? null
                  : (
                    <Tooltip classes={{ popper: styles.tooltipContainer, tooltip: styles.tooltipContent }}
                title={
                (<Fragment>
                {"On soustrait le montant après amendement du montant avec PLF"}<br/>
                {" et on le divise par le RFR moyen qui est de"}<br />
                <strong>{`${row.rfrMoyenFoyer} €`}</strong>
                {"  /an pour ce décile."}</Fragment>)}>
                    <span className={styles.reform}>
                      {`${row.ratioEcartApresMoinsPlfSurRfr} ‰`}
                    </span></Tooltip>)
              }
              {
                // If reform and NO PLF, display diff with avant
                row.ratioEcartApresMoinsAvantSurRfr === undefined
                ? null
                : (row.ratioEcartPlfMoinsAvantSurRfr === undefined)
                  ? ( <Tooltip classes={{ popper: styles.tooltipContainer, tooltip: styles.tooltipContent }}
                    title={
                      (<Fragment>
                      {"On soustrait le montant après amendement du montant de la loi en vigueure"}<br/>
                      {" et on le divise par le RFR moyen qui est de"}<br />
                      <strong>{`${row.rfrMoyenFoyer} €`}</strong>
                      {"  /an pour ce décile."}</Fragment>)}>
                    <span className={styles.reform}>
                    {`${row.ratioEcartApresMoinsAvantSurRfr} ‰`}
                    </span></Tooltip>)
                  : null
              }

              </div>

            </TableCell>

          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
}
