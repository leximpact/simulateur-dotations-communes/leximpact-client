import Tooltip from "@material-ui/core/Tooltip";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import { Fragment, PureComponent } from "react";

import { SubCard, Values } from "../../../../common";
import styles from "./GagnantsPerdantsContent.module.scss";

interface Props {
  icon: JSX.Element;
  title: string;
  plf?: number;
  amendement?: number;
  caption?: string;
  captionPlf?: number;
  captionAmendement?: number;
}

function inMillions(n: number|undefined): number|undefined {
  if (n === undefined) {
    return undefined;
  }
  return Math.round(n / 100000) / 10;
}

export class GagnantsPerdantsContent extends PureComponent<Props> {
  render() {
    const {
      amendement, caption, captionAmendement, captionPlf, icon, plf, title,
    } = this.props;

    const amendementInMillions = inMillions(amendement);
    const plfInMillions = inMillions(plf);
    const help = (
      <Fragment>
        <strong>
          {" "}
          {`${amendementInMillions} million(s)`}
          {" "}
        </strong>
        {" de foyers fiscaux sont impactés par mon amendement. \
          Ce nombre, en million(s), est identique au nombre de foyers fiscaux impactés par le PLF."}
        <br />
        <br />
        {"Mon amendement et le PLF peuvent néanmoins cibler des situations de foyers fiscaux différentes."}
      </Fragment>
    );
    return (
      <SubCard
        icon={icon}
        title={title}
      >
        <div className={styles.containerImpact}>

          <div className={styles.main}>
            <span className={styles.mainValue}>
              <Values amendementValue={amendementInMillions} plfValue={plfInMillions} />
            </span>
            <span className={styles.mainUnit}> M</span>
            {
              (
                (typeof amendementInMillions === "number" && typeof plfInMillions === "number") && (
                  amendementInMillions === plfInMillions
                )
              ) && (
                <Tooltip
                  classes={{
                    popper: styles.tooltipContainer,
                    tooltip: styles.tooltipContent,
                  }}
                  title={help || <span />}>
                  <HelpOutlineIcon className={styles.tooltipButton} fontSize="small" />
                </Tooltip>
              )
            }
          </div>
        </div>
        {
          caption && (typeof amendement === "number" || typeof plf === "number") && (
            <div className={styles.details}>
              dont
              {" "}
              <span>
                <Values
                  amendementValue={inMillions(captionAmendement)}
                  plfValue={inMillions(captionPlf)} />
              </span>
              <span className={styles.detailsUnit}> M</span>
              {" "}
              {caption}
            </div>
          )
        }
      </SubCard>
    );
  }
}
