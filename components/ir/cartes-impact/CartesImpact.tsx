import Grid from "@material-ui/core/Grid";
import { PureComponent } from "react";

// eslint-disable-next-line no-unused-vars
import { CasType } from "../../../redux/reducers/descriptions/ir";
import { InformationPanel } from "../../common";
import { CarteEtat } from "./carte-etat";
import styles from "./CartesImpact.module.scss";
import { GagnantsPerdantsCard } from "./gagnants-perdants";
import SimpleCard from "./simple-card";

export const INFORMATION_PANEL_NAME = "ir";

interface Props {
  casTypes: CasType[];
  isInformationPanelVisible: boolean;
  isUserLogged: boolean;
}

export class CartesImpact extends PureComponent<Props> {
  render() {
    const { casTypes, isInformationPanelVisible, isUserLogged } = this.props;
    return (
      <div className={styles.container}>
        <Grid container spacing={3}>
          {/*isInformationPanelVisible && isUserLogged && (
            <Grid item lg={8} md={12} sm={12} xl={9} xs={12}>
              <InformationPanel name={INFORMATION_PANEL_NAME} title="⚠️ Incidence de la crise sanitaire sur les résultats macros">
                Les estimations de Leximpact des effets sur
                le budget de l&apos;État sont calculées à partir de données recalibrées
                s&apos;appuyant sur des enquêtes de l&apos;année 2018.
                L&apos;épidémie actuelle affectant l&apos;économie dans une mesure qui
                est à ce jour difficile à prévoir, les résultats des deux premiers panneaux
                &quot;Recettes de l&apos;État&quot; et &quot;Nombre de foyers fiscaux&quot; sont
                probablement surestimés voir erronés.
              </InformationPanel>
            </Grid>
          )*/}
          {isUserLogged && (
            /*
            xl: The number of columns to span on extra large devices (≥1200px)
            lg: The number of columns to span on large devices (≥992px)
            md: The number of columns to span on medium devices (≥768px)
            sm: The number of columns to span on small devices (≥576px)
            xs: The number of columns to span on extra small devices (<576px)
            */
            <Grid item xs={"auto"} sm={12} md={12} lg={12} xl={10}>
              <CarteEtat />
            </Grid>
          )}
          {isUserLogged && (
            <Grid item xs={"auto"} sm={6} md={6} lg={6} xl={3}>
              <GagnantsPerdantsCard />
            </Grid>
          )}
        </Grid>
        <div className={styles.partiecastype}>
          <span className={styles.titrepartie}>Impacts sur des cas types</span><br/>
          <span className={styles.soustitrepartie}>À revenus identiques 2021 et 2022</span>
        </div>
        <Grid container spacing={3}>
          {casTypes.map((casType, index) => {
            const itemKey = `react::simple-card-key-index::${index}`;
            return (
              <Grid key={itemKey} item lg={4} md={6} sm={6} xl={3} xs={12}>
                <SimpleCard index={index} />
              </Grid>
            );
          })}
        </Grid>
      </div>
    );
  }
}
