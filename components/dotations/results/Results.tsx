import Grid from "@material-ui/core/Grid";
import { PureComponent } from "react";
// eslint-disable-next-line no-unused-vars
import { connect, ConnectedProps } from "react-redux";

// eslint-disable-next-line no-unused-vars
import { RootState } from "../../../redux/reducers";
import { InformationPanel } from "../../common";
import { CommuneSearch } from "./commune-search";
import { CommuneStrateDetails } from "./commune-strate-details";
import { CommuneSummary } from "./commune-summary";
import { CommuneType } from "./commune-type";
import styles from "./Results.module.scss";

const INFORMATION_PANEL_NAME = "dotations";

const mapStateToProps = ({ descriptions, display }: RootState) => ({
  communesTypes: descriptions.dotations.communesTypes,
  isInformationPanelVisible: display.currentInformationPanels.includes(
    INFORMATION_PANEL_NAME
  ),
});

const connector = connect(mapStateToProps);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {};

class Results extends PureComponent<Props> {
  render() {
    const { communesTypes, isInformationPanelVisible } = this.props;
    return (
      <div className={styles.container}>
        {isInformationPanelVisible && (
          <Grid container spacing={3}>
            <Grid item lg={12} xl={8}>
              <InformationPanel
                name={INFORMATION_PANEL_NAME}
                title="⚠️ Ce simulateur n'est plus à jour depuis 2021."
              >
                Les dotations présentées par cette application s'appuient sur
                les données et modèles de fin 2021 et n'ont pas été mises à jour
                depuis.
                <br />
                <b>Vous seriez intéressé que LexImpact mette à jour ce simulateur ?</b>
                Faites-le nous savoir en{" "}
                <a className={styles.a} 
                  href="https://limesurvey.leximpact.dev/index.php/449167?lang=fr"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  <b>complétant ce formulaire</b>
                </a>
                .
                <br />
                <br />
                Les montants des dotations effectivement versées sont publiés par la<a 
                  className={styles.aGray}
                  href="http://www.dotations-dgcl.interieur.gouv.fr/consultation/dotations_en_ligne.php"
                  rel="noopener noreferrer"
                  target="_blank"
                  >
                    DGCL
                </a>.
                Leurs évolutions sont consultables sur <a 
                  className={styles.aGray}
                  href="http://dotations.incubateur.anct.gouv.fr"
                  rel="noopener noreferrer"
                  target="_blank"
                  >une interface dédiée de l'ANCT</a>.
              </InformationPanel>
            </Grid>
          </Grid>
        )}
        <Grid container spacing={3}>
          <Grid item lg={12} xl={8}>
            <CommuneStrateDetails />
          </Grid>
          <Grid item lg={8} md={12} sm={12} xl={4} xs={12}>
            <CommuneSummary />
          </Grid>
          <Grid item lg={4} md={6} sm={6} xl={3} xs={12}>
            <CommuneSearch />
          </Grid>
          {/* </Grid>
        <Grid container spacing={3}> */}
          {communesTypes.map((communeType, index) => (
            <Grid
              key={communeType.code}
              item
              lg={4}
              md={6}
              sm={6}
              xl={3}
              xs={12}
            >
              <CommuneType
                code={communeType.code}
                departement={communeType.departement}
                habitants={communeType.habitants}
                index={index}
                name={communeType.name}
                potentielFinancierParHab={communeType.potentielFinancierParHab}
              />
            </Grid>
          ))}
        </Grid>
      </div>
    );
  }
}

const ConnectedResults = connector(Results);

export { ConnectedResults as Results };
