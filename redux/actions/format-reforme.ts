import merge from "lodash/merge";

export function formatReforme(reforme) {
  return merge({}, reforme, {
    impot_revenu: {
      bareme_ir_depuis_1945: {
        taux: reforme.impot_revenu.bareme_ir_depuis_1945.taux.map(
          (taux) => taux / 100
        ),
      },
      calcul_impot_revenu: {
        plaf_qf: {
          decote: {
            taux: reforme.impot_revenu.calcul_impot_revenu.plaf_qf.decote.taux / 100,
          },
          abat_dom: {
            taux_GuadMarReu:
              reforme.impot_revenu.calcul_impot_revenu.plaf_qf.abat_dom.taux_GuadMarReu / 100,
            taux_GuyMay:
              reforme.impot_revenu.calcul_impot_revenu.plaf_qf.abat_dom.taux_GuyMay / 100,
          },
        },
      },
    },
  });
}
