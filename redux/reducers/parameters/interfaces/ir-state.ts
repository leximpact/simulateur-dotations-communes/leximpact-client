/* eslint-disable camelcase */
export interface IRState {
    bareme_ir_depuis_1945: {
      seuils: number[];
      taux: number[];
    };
    calculNombreParts: {
      partsSelonNombrePAC: {
        veuf: number;
        mariesOuPacses: number;
        celibataire: number;
        divorce: number;
      }[];
      partsParPACAuDela: number;
      partsParPACChargePartagee: {
        zeroChargePrincipale: {
          deuxPremiers: number;
          suivants: number;
        };
        unChargePrincipale: {
          premier: number;
          suivants: number;
        };
        deuxOuPlusChargePrincipale: {
          suivants: number;
        };
      };
      bonusParentIsole: {
        auMoinsUnChargePrincipale: number;
        zeroChargePrincipaleUnPartage: number;
        zeroChargeprincipaleDeuxOuPlusPartage: number;
      };
    };
    calcul_impot_revenu: {
      plaf_qf: {
        decote: {
          seuil_celib: number;
          seuil_couple: number;
          taux: number;
        };
        abat_dom: {
          plaf_GuadMarReu: number;
          plaf_GuyMay: number;
          taux_GuadMarReu: number;
          taux_GuyMay: number;
        };
        celib: number;
        celib_enf: number;
        general: number;
        reduc_postplafond: number;
        reduc_postplafond_veuf: number;
      };
    };
}
