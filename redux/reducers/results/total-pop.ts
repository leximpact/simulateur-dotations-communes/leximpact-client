/* eslint-disable camelcase */
import { get } from "lodash";
// TODO: PLF2023
/**
  To update the DEFAULT_STATE below, use leximpact-server Web API response
  as its format should be identical to the State interface below:
  * if the "base" (= "avant") law is updated, set the compulsory attributes of the State interface to Web API values,
  * if the "plf" is activated, add all the "plf" optional attributes of the State interface
    and set them to Web API values.
  * and do not set any "apres" attribute as the DEFAULT_STATE should NOT include any "amendement".
*/
interface State {
  deciles: {
    sum_rfr: number;
    avg_rfr: number;
    poids: number;
    plf?: number;
    sum_irpp_plf?: number;
    avg_irpp_plf?: number;
    irpp_on_rfr_plf?: number;
    avg_irpp_on_avg_rfr_plf?: number;
    avant: number;
    sum_irpp_avant: number;
    avg_irpp_avant: number;
    irpp_on_rfr_avant: number;
    avg_irpp_on_avg_rfr_avant: number;
    apres?: number;
    sum_irpp_apres?: number;
    avg_irpp_apres?: number;
    irpp_on_rfr_apres?: number;
    avg_irpp_on_avg_rfr_apres?: number;
  }[];
  foyersFiscauxTouches: {
    avant_to_plf?: {
      neutre?: number;
      neutre_zero?: number;
      gagnant?: number;
      perdant?: number;
      perdant_zero?: number;
    };
    avant_to_apres?: {
      neutre?: number;
      neutre_zero?: number;
      gagnant?: number;
      perdant?: number;
      perdant_zero?: number;
    };
    plf_to_apres?: {
      neutre?: number;
      neutre_zero?: number;
      gagnant?: number;
      perdant?: number;
      perdant_zero?: number;
    };
  };
  frontieresDeciles: number[];
  total: {
    apres?: number;
    avant: number;
    plf?: number;
  };
}

const DEFAULT_STATE: State = {
  deciles: [
    {
      sum_rfr: 1248595294.3250895,
      avg_rfr: 325.6424932857625,
      poids: 3834251.733324631,
      avant: 0,
      sum_irpp_avant: 0,
      avg_irpp_avant: 0,
      irpp_on_rfr_avant: 0,
      avg_irpp_on_avg_rfr_avant: 0,
      plf: 0.0,
      sum_irpp_plf: 0.0,
      avg_irpp_plf: 0.0,
      irpp_on_rfr_plf: 0.0,
      avg_irpp_on_avg_rfr_plf: 0.0,
    },
    {
      sum_rfr: 23436330862.321556,
      avg_rfr: 6114.03411542516,
      poids: 3833202.5009795064,
      avant: 0,
      sum_irpp_avant: 0,
      avg_irpp_avant: 0,
      irpp_on_rfr_avant: 0,
      avg_irpp_on_avg_rfr_avant: 0,
      plf: 0.0,
      sum_irpp_plf: 0.0,
      avg_irpp_plf: 0.0,
      irpp_on_rfr_plf: 0.0,
      avg_irpp_on_avg_rfr_plf: 0.0,
    },
    {
      sum_rfr: 43302552094.83705,
      avg_rfr: 11298.140156590667,
      poids: 3832715.074753033,
      avant: 0,
      sum_irpp_avant: 0,
      avg_irpp_avant: 0,
      irpp_on_rfr_avant: 0,
      avg_irpp_on_avg_rfr_avant: 0,
      plf: 0.0,
      sum_irpp_plf: 0.0,
      avg_irpp_plf: 0.0,
      irpp_on_rfr_plf: 0.0,
      avg_irpp_on_avg_rfr_plf: 0.0,
    },
    {
      sum_rfr: 59109327425.504074,
      avg_rfr: 15420.218819854104,
      poids: 3833235.320201719,
      avant: 163391066.7322619,
      sum_irpp_avant: 163391066.7322619,
      avg_irpp_avant: 42.62484639833265,
      irpp_on_rfr_avant: 0.002764217998220076,
      avg_irpp_on_avg_rfr_avant: 0.002764217998220076,
      plf: 55561764.28025984,
      sum_irpp_plf: 55561764.28025984,
      avg_irpp_plf: 14.494743901435179,
      irpp_on_rfr_plf: 0.0009399829781521129,
      avg_irpp_on_avg_rfr_plf: 0.0009399829781521129,
    },
    {
      sum_rfr: 72143223721.7203,
      avg_rfr: 18819.276995759777,
      poids: 3833474.7789712735,
      avant: 944751019.7222807,
      sum_irpp_avant: 944751019.7222807,
      avg_irpp_avant: 246.4476941141624,
      irpp_on_rfr_avant: 0.013095492147211086,
      avg_irpp_on_avg_rfr_avant: 0.013095492147211086,
      plf: 814866633.9530004,
      sum_irpp_plf: 814866633.9530004,
      avg_irpp_plf: 212.56606106370742,
      irpp_on_rfr_plf: 0.011295123007126496,
      avg_irpp_on_avg_rfr_plf: 0.011295123007126496,
    },
    {
      sum_rfr: 87683818162.51213,
      avg_rfr: 22873.391349911217,
      poids: 3833441.959749177,
      avant: 1703341992.9176059,
      sum_irpp_avant: 1703341992.9176059,
      avg_irpp_avant: 444.3374937725895,
      irpp_on_rfr_avant: 0.019425955992936488,
      avg_irpp_on_avg_rfr_avant: 0.01942595599293649,
      plf: 1677008280.465949,
      sum_irpp_plf: 1677008280.465949,
      avg_irpp_plf: 437.46802431716037,
      irpp_on_rfr_plf: 0.019125628056201122,
      avg_irpp_on_avg_rfr_plf: 0.019125628056201126,
    },
    {
      sum_rfr: 110115965949.4268,
      avg_rfr: 28726.174096337054,
      poids: 3833297.3120660484,
      avant: 3031830109.6376085,
      sum_irpp_avant: 3031830109.6376085,
      avg_irpp_avant: 790.919634669199,
      irpp_on_rfr_avant: 0.02753306555953969,
      avg_irpp_on_avg_rfr_avant: 0.027533065559539695,
      plf: 2863779561.8641014,
      sum_irpp_plf: 2863779561.8641014,
      avg_irpp_plf: 747.0799493819293,
      irpp_on_rfr_plf: 0.026006939374982215,
      avg_irpp_on_avg_rfr_plf: 0.026006939374982215,
    },
    {
      sum_rfr: 137184876883.36745,
      avg_rfr: 35787.99053345202,
      poids: 3833265.708370462,
      avant: 5920010075.927875,
      sum_irpp_avant: 5920010075.927875,
      avg_irpp_avant: 1544.3776994119555,
      irpp_on_rfr_avant: 0.04315351816046735,
      avg_irpp_on_avg_rfr_avant: 0.04315351816046735,
      plf: 5810007005.203121,
      sum_irpp_plf: 5810007005.203121,
      avg_irpp_plf: 1515.6807399278928,
      irpp_on_rfr_plf: 0.04235158541360509,
      avg_irpp_on_avg_rfr_plf: 0.04235158541360509,
    },
    {
      sum_rfr: 179011128573.3233,
      avg_rfr: 46701.02290233606,
      poids: 3833130.7849014252,
      avant: 11081534982.670897,
      sum_irpp_avant: 11081534982.670897,
      avg_irpp_avant: 2890.98796897856,
      irpp_on_rfr_avant: 0.061904168031273424,
      avg_irpp_on_avg_rfr_avant: 0.061904168031273424,
      plf: 11432749052.604158,
      sum_irpp_plf: 11432749052.604158,
      avg_irpp_plf: 2982.613872095849,
      irpp_on_rfr_plf: 0.06386613320310294,
      avg_irpp_on_avg_rfr_plf: 0.06386613320310294,
    },
    {
      sum_rfr: 379094672499.9683,
      avg_rfr: 98903.84763576475,
      poids: 3832961.8266832978,
      avant: 59557996094.47585,
      sum_irpp_avant: 59557996094.47585,
      avg_irpp_avant: 15538.374444499754,
      irpp_on_rfr_avant: 0.15710586408855648,
      avg_irpp_on_avg_rfr_avant: 0.15710586408855648,
      plf: 64246027701.62953,
      sum_irpp_plf: 64246027701.62953,
      avg_irpp_plf: 16761.457746430395,
      irpp_on_rfr_plf: 0.16944695903842485,
      avg_irpp_on_avg_rfr_plf: 0.16944695903842483,
    },
  ],

  foyersFiscauxTouches: {
    avant_to_plf: {
      neutre_zero: 17078623,
      perdant: 143587,
      gagnant: 15417831,
    },
    avant_to_apres: {
      neutre_zero: 17078623,
      perdant: 143587,
      gagnant: 15417831,
    },
    plf_to_apres: {
      neutre_zero: 18029380,
      gagnant: 14610661,
    },
  },
  frontieresDeciles: [
    2365.825439453125, 9034.31640625, 13487.224609375, 17108.0, 20664.244140625,
    25757.998046875, 31956.744140625, 40103.5703125, 55275.3984375, 8615171.0,
  ],
  total: {
    //avant: 82400000000.00001, PLF 2022
    avant: 86800000000.00001, // PLF 2023, chiffre révisé 2022 (page 15 du PLF2023)
    plf: 86887586871.0001, // PLF2023 page 136
  },
};

const totalPop = (state: State = DEFAULT_STATE, action): State => {
  switch (action.type) {
    case "onSimPopFetchResult":
      return {
        deciles: get(action, "data.deciles"),
        foyersFiscauxTouches: get(action, "data.foyers_fiscaux_touches"),
        frontieresDeciles: get(action, "data.frontieres_deciles"),
        total: get(action, "data.total"),
      };
    default:
      return state;
  }
};

export default totalPop;
