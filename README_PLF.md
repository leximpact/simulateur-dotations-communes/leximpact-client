# Activation & Désactivation du mode PLF

Pour l'[application IR](http://leximpact.an.fr/ir) et l'[application dotations](http://leximpact.an.fr/dotations), il est possible d'activer un mode où, en plus de la loi en vigueur, un Projet de Loi de Finances (PLF) sert de base aux calculs d'amendements.

Cette documentation présente les éléments à mettre à jour pour activer et désactiver le mode `PLF`. 

> ⚠️ Cette documentation n'est pas exhaustive et s'apparente à ce stade à une prise de notes.

## Logique d'activation & désactivation du PLF

`leximpact-client` échange par API Web avec `leximpact-server`.

Les deux dépôts `leximpact-client` et `leximpact-server` disposent d'éléments de configuration qui déterminent l'état initial de ces applications.

Néanmoins, pour le couple `leximpact-client`+`leximpact-server` la logique générale veut que ce soit le client qui sache si le PLF est activé.
> Aux débuts de ces applications (pré-PLF 2021), l'activation du PLF était décidée par `leximpact-server`. 

TODO Vérifier si le client détermine l'application du PLF d'après la première requête cas type ou si ce comportement est désactivé.

## Contexte métier

`leximpact-client` connaît potentiellement 3 lois : 
* la loi en vigueur, dite `base` dans l'état de l'application, `avant` dans les requêtes à l'API Web,
* la loi réformée par un usager de l'application, dite `amendement` dans l'état de l'application, `apres` dans les requêtes à l'API Web,
* et, lorque le PLF est activé, la loi réformée par un PLF, dite `plf` dans l'état de l'application et les requêtes à l'API Web.

Pour l'[application IR](http://leximpact.an.fr/ir), nous effectuons des calculs sur cas type et population. 

### Calcul sur cas types

Au démarrage de l'application, une requête http://localhost:5000/metadata/description_cas_types est transmise (`onInitializeCasTypes` redux state ? => SIMULATE_CAS_TYPES_REQUEST => SIMULATE_CAS_TYPES_FAILURE)

Un clic sue "ESTIMER CAS TYPES" produit une requête http://localhost:5000/calculate/compare 
redux/actions/simulations/simulate-cas-types.ts

Les cas types de l'usager sont sauvegardés sur son navigateur (cookies).

🔥 CartesImpact props, extrait d'un print des props dans la console :
```js 
{casTypes: Array(6), isInformationPanelVisible: true, isUserLogged: false, dispatch: ƒ}
casTypes: Array(6)
0:
    declarants: Array(1)
        0: {ancienCombattant: false, invalide: false, parentIsole: false, retraite: false, veuf: false, …}
        length: 1
        [[Prototype]]: Array(0)
    name: "Foyer fiscal type"
    personnesACharge: Array(0)
        length: 0
        [[Prototype]]: Array(0)
    residence: "metropole"
    revenuImposable: 15584
```

Et, du state aux properties d'une SimpleCard : components/ir/cartes-impact/simple-card/index.ts

La zone impact d'une carte de cas type (bas de la carte) : components/ir/cartes-impact/simple-card/impact-impots.tsx - SimpleCardImpactImpots

Reducers dédiés : redux/reducers/results
Calcul d'une requête : redux/actions/simulations/simulate-cas-types.ts

### Calcul sur population

Avant, cela était défini dans redux/reducers/results/total-pop.ts
Maintenant, c'est récupéré de la requête au serveur lorsque celui-ci a le PLF activé.


### Désactivation du PLF

Pour désactiver le PLF, contrairement à `leximpact-server`, pour `lex-impact-client` le `.env` de l'application reste inchangé.

Mais pour l'IR, il s'agit de :
* Mettre à jour l'état par défaut `BASE_IR_DEFAULT_STATE` défini dans `./redux/reducers/parameters/base/ir.ts`
* Vérifier la cohérence des montants sur les déciles de population définis dans `redux/reducers/results/total-pop.ts`
* S'assurer de la cohérence du message d'information sur la carte budget `components/ir/cartes-impact/carte-etat/CarteEtat.tsx`
* Mettre à jour le reducer en cas de `SIMULATE_CAS_TYPES_SUCCESS` défini dans `redux/reducers/results/plf/ir.ts`

### Activation du PLF

Pour activer le PLF, contrairement à `leximpact-server`, pour `lex-impact-client` le `.env` de l'application reste inchangé.

Mais pour l'IR, il s'agit de :
* Mettre à jour l'état présentant le PLF `PLF_IR_DEFAULT_STATE` défini dans `redux/reducers/parameters/plf/ir.ts` et qui diffère désormais de `BASE_IR_DEFAULT_STATE`
* Vérifier la cohérence des montants sur les déciles de population définis dans `redux/reducers/results/total-pop.ts`
* S'assurer de la cohérence du message d'information sur la carte budget `components/ir/cartes-impact/carte-etat/CarteEtat.tsx`
* Mettre à jour le reducer en cas de `SIMULATE_CAS_TYPES_SUCCESS` défini dans `redux/reducers/results/plf/ir.ts`

## Debug

Un principe directeur de l'application est que l'interface graphique affichée dépend d'un état de l'application. Cet état est décrit dans [cette section du README](https://git.leximpact.dev/leximpact/leximpact-client/-/blob/master/README.md#organisation-de-létat-de-lapplication).


Le RootState est l'interface de l'état global redux. Elle est déduite des réduceurs. De cette manière, on peut déduire des bugs en amont en l'utilisant à chaque fois dans les mapStateToProps. ([src](https://github.com/leximpact/leximpact-client/pull/46))

Le root state est défini ici : redux/reducers/index.ts
L'état est construit ici : redux/make-application-state.ts

Les interfaces des états redux se situent maintenant dans le dossier `./redux/reducers/` avec leur réduceur associé ([src](https://github.com/leximpact/leximpact-client/pull/46)). = Les interfaces d'action se situent maintenant conjointement avec leur fonction créatrice.

Le format de ce state est défini ici (selon la partie gauche, la réforme paramétrique) : redux/reducers/parameters/interfaces/ir-state.ts
Interface total-pop : redux/reducers/results/total-pop.ts

L'arborescence des états de l'application est contenue dans un store ([en savoir plus sur un Redux store](https://redux.js.org/api/store))

Des outils de développement facilitent grandement le debug de l'application comme : 
* React via React Developer Tools [plugin chrome](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)
* Redux via Redux DevTools [plugin chrome](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=fr) pour suivre l'évolution de l'état de l'application

> Pour accéder aux calculs sur population, en local, prendre l'URL /connection/token et l'ajouter à [localhost](http://localhost:9001/) (sans `ir/`)

### FAQ développement

#### Bug des tirets - Sur les cas types, pourquoi les valeurs sont-elles remplacées par des tirets ?

Au chargement de l'interface IR, l'impôt et le nombre de parts des cas types n'est pas représenté par un nombre mais par un `-`.
Ceci intervient dans le composant components/common/articles-inputs/values/Values.tsx  

Lorsque le PLF est désactivé du côté de leximpact-server seulement alors, on passe de : 
`SimpleCardImpactImpots props {index: 5, amendement: 690, base: 756, isFetching: false, plf: 690, …}`
à `SimpleCardImpactImpots props {index: 5, amendement: undefined, base: undefined, isFetching: true, plf: undefined, …}`.

Ceci serait lié à la présence du `PLF_PATH` seulement. Le client nécessiterait toujours la présence du PLF pour fonctionner (ou une modification de son code). Ceci est en partie apparu avec [cette PR](https://github.com/leximpact/leximpact-client/pull/104).

#### Bug npm run dev - En mode dev ma modification n'est pas visible, que faire ?

Si une modification du code source n'entraîne pas de changement dans l'interface en mode développement, il s'agit d'une situation déjà rencontrée. Relancer le `npm run dev`.
