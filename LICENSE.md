# LexImpact-client

By [Sandra Chakroun](mailto:sandra.chakroun@assemblee-nationale.fr)
By [Benoît Courty](mailto:benoit.courty@assemblee-nationale.fr),
By [Dorine Lambinet](mailto:dorine.lambinet@assemblee-nationale.fr)
By Loïc Poullain,
By Augustin Wenger
By Mauko Quiroga

Copyright (C) 2019, 2020, 2021, 2022, 2023 Assemblée nationale

https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-client/

> LexImpact-client LexImpact is free software; you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> LexImpact-client LexImpact is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.

